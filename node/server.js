// server/index.js

const express = require("express");
const cors = require('cors');
const bodyParser = require('body-parser');

const PORT = process.env.PORT || 3001;

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.post('/submit', (req, res) => {
    console.log('Form data:', (req.body));
    if(res.body != ''){
        res.json({ message: 'success' });
    }
        
    
  });

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});