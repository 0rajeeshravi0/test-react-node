import './App.css';
import Grid from '@mui/material/Grid';
import Divider from '@mui/material/Divider';
import React from 'react';
import { BrowserRouter as Router, Route, Link, Routes } from 'react-router-dom';
import Login from './login';
import Home from './home';
function App() {

  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Login />} />
      </Routes>
      <Routes>
        <Route exact path="/home" element={<Home />} />
      </Routes>
      
    </Router>
  );
}

export default App;
