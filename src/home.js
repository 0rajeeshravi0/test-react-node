import './App.css';
import Grid from '@mui/material/Grid';
import Divider from '@mui/material/Divider';
import React from 'react';
import { useState } from 'react';
import Button from '@mui/material/Button';
import { BrowserRouter as Router, Route, Link, Routes } from 'react-router-dom';
import Login from './login';

function Home() {
    const [dat, setdat] = useState('true');
    const [logout, setLogout] = useState(false);
// async function directhit(){
//     const dat = await fetch('http://localhost:3001/api')
//     .then(response => {
//       if (!response.ok) {
//         throw new Error('Network response was not ok');
//       }
//       return response.json();
//     })
//     .then(data => {
//     //   console.log('Server data:', data);
//     return data;
//     })
//     .catch(error => {
//       console.error('Error fetching data:', error);
//     });
//     setdat(dat.message);
// }

// directhit();
function flogout(){
    setLogout('true');
    console.log(logout);
}
if(logout){
    window.location.href = '/'
}else{
    return (
        
        (dat != 'noaccess') ? 
        <>
        <Button variant="contained" size="small" style={{position:"absolute", marginLeft:"80%"}} onClick={flogout}>Logout</Button>
    <Grid container spacing={0}>
        <Grid xs={1}>
          <div style={{ background: "#0d61b6", height: "113%" }}>
            <Grid container>
              <Grid xs={12}>
                <p className='rotatetext'>Self Help</p>
              </Grid>
              <Grid xs={12} className="mt-15">
                <p className='rotatetext'>Non - Operative</p>
              </Grid>
              <Grid xs={12} className="mt-15">
                <p className='rotatetext'>All</p>
              </Grid>
              <Grid xs={12} style={{ marginTop: "250px" }}>
                <p className='rotatetext'>Observation / No Treatement</p>
              </Grid>
            </Grid>
          </div>
        </Grid>
        <Grid xs={11}>
          <Grid container spacing={-1}>
            <Grid xs={8} display="flex" alignItems="center" justifyContent="right">
              <div className="outer gradient"><p className="inner" style={{ maxWidth: "200px" }}>Activity Modification</p></div>
            </Grid>
            <Grid xs={8} display="flex" alignItems="center" justifyContent="right" style={{ marginTop: "-30px" }}>
              <div className="outer gradient"><p className="inner" style={{ maxWidth: "150px" }}>Simple Excercises</p></div>
            </Grid>
            <Grid xs={8} display="flex" alignItems="center" justifyContent="right" style={{ marginTop: "-30px" }}>
              <div className="outer gradient" style={{ width: "425px" }}><p className="inner" style={{ maxWidth: "250px" }}>Walking Aid / Simple Pain Killers</p></div>
            </Grid>
            <Divider
              style={{ width: "100%" }}
            />
            <Grid xs={12} display="flex" alignItems="center" justifyContent="right" style={{ marginTop: "0px" }}>
              <p className="outer gradient" style={{ width: "100%" }}><p className="inner" style={{ maxWidth: "150px" }}>wellbeing</p></p>
            </Grid>
            <Grid xs={12} display="flex" alignItems="center" justifyContent="right" style={{ marginTop: "-30px" }}>
              <p className="outer gradient" style={{ width: "938px" }}><p className="inner" style={{ maxWidth: "400px" }}>Physiotherapy / Structure Excercise Programmes</p></p>
            </Grid>
            <Grid xs={8} display="flex" alignItems="center" justifyContent="right" style={{ marginTop: "-30px" }}>
              <p className="outer gradient" style={{ width: "425px" }}><p className="inner" style={{ maxWidth: "250px" }}>hip replacement</p></p>
            </Grid>
            <Divider
              style={{ width: "100%" }}
            />
            <Grid xs={7} display="flex" alignItems="center" justifyContent="right" style={{ marginTop: "0" }}>
              <p className="outer gradient" style={{ width: "300px" }}><p className="inner" style={{ maxWidth: "220px" }}>Glucosamine & chondroitin</p></p>
            </Grid>
            <Grid xs={8} display="flex" alignItems="center" justifyContent="right" style={{ marginTop: "-30px" }}>
              <p className="outer gradient" style={{ width: "300px" }}><p className="inner" style={{ maxWidth: "170px" }}>Surface Replacement</p></p>
            </Grid>
            <Grid xs={8} display="flex" alignItems="center" justifyContent="right" style={{ marginTop: "-30px" }}>
              <p className="outer gradient" style={{ width: "300px" }}><p className="inner" style={{ maxWidth: "170px" }}>Stem Cell Treatment</p></p>
            </Grid>
            <Grid xs={6} display="flex" alignItems="center" justifyContent="right" style={{ marginTop: "-30px" }}>
              <p className="outer gradient" style={{ width: "350px" }}><p className="inner" style={{ maxWidth: "230px" }}>FAI (impingement) surgery</p></p>
            </Grid>
            <Grid xs={7} display="flex" alignItems="center" justifyContent="right" style={{ marginTop: "-30px" }}>
              <p className="outer gradient" style={{ width: "300px" }}><p className="inner" style={{ maxWidth: "220px" }}>Labral Repair</p></p>
            </Grid>
            <Grid xs={7} display="flex" alignItems="center" justifyContent="right" style={{ marginTop: "-30px" }}>
              <p className="outer gradient" style={{ width: "300px" }}><p className="inner" style={{ maxWidth: "260px" }}>Osteotomies around the hip joint</p></p>
            </Grid>
            <Grid xs={12} display="flex" alignItems="center" justifyContent="right" style={{ marginTop: "0px" }}>
              <p className="outer gradient" style={{ width: "100%" }}><p className="inner" style={{ maxWidth: "200px" }}>Alternative Medicine</p></p>
            </Grid>
            <Divider
              style={{ width: "100%" }}
            />
            <Grid xs={12} display="flex" alignItems="center" justifyContent="right" style={{ marginTop: "0px" }}>
              <p className="outer gradient" style={{ width: "100%" }}><p className="inner" style={{ maxWidth: "300px" }}>Observation or no active treatment</p></p>
            </Grid>
          </Grid>
        </Grid>
      </Grid></> : 'no access'
       );
    }
    }
    
    export default Home;