import './login.css';
import React from "react";
import { useState } from 'react';

function Login() {
    const [userName, setUserName] = useState('');
    const [Pass, setPass] = useState('');
    function handleChange(event) {
        setUserName(event.target.value);
    }

   async function submit() {
       await fetch('http://localhost:3001/submit',{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({ name: userName, email: Pass })
          })
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.json();
    })
    .then(data => {
      console.log('Server data:', data);
      if(data.message == 'success'){
        window.location.href = "/home";
      }
    })
    .catch(error => {
      console.error('Error fetching data:', error);
    });
    }

    return (
        <>
            <body>

                <body class="page">
                    <main class="main page__main">
                        <form class="login-form main__login-form" action="javascript:void(0)" method="POST">
                            <h3 class="login-form__title">Sign In</h3>
                            <label class="login-form__label" for="uname"><span class="sr-only">Username</span>
                                <input class="login-form__input" type="text" id="uname" name="uname" value={userName} onChange={handleChange} placeholder="Username" required="required" />
                            </label>
                            <label class="login-form__label" for="psw"><span class="sr-only">Password</span>
                                <input class="login-form__input" type="password" id="psw" name="psw" value={Pass} onChange={(e) => { setPass(e.target.value) }} placeholder="Password" required="required" />
                            </label>
                            <button class="primary-btn" type="submit" onClick={submit}>Login</button>
                            <div class="login-form__footer"><a class="login-form__link" href="#">Forget Password?</a><a class="login-form__link" href="#">Sign Up</a></div>
                        </form>
                    </main>
                </body>
            </body>
        </>
    );
}

export default Login;